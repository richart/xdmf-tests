/* -------------------------------------------------------------------------- */
// #include <XdmfDomain.hpp>
// #include <XdmfGridCollection.hpp>
// #include <XdmfInformation.hpp>
// #include <XdmfWriter.hpp>
// #include <boost/shared_ptr.hpp>
#include "aka_iterators.hh"
#include <dumper_xdmf.hh>
#include <solid_mechanics_model.hh>
/* -------------------------------------------------------------------------- */

using namespace akantu;

template <class T> struct average {
  template <class Derived>
  Vector<T> operator()(const Eigen::MatrixBase<Derived> &in,
                       const ElementType &type,
                       const GhostType &ghost_type) const {
    auto nb_points = nb_averaging(type, ghost_type);
    auto nb_component = in.size() / nb_averaging(type, ghost_type);
    Vector<T> out(nb_component);
    out.zero();
    MatrixProxy<const typename Derived::Scalar> in_mat(in.derived().data(),
                                                       nb_component, nb_points);
    for (auto &&in_point : in_mat) {
      out += in_point;
    }
    out /= nb_points;
    return out;
  }

  void setNbIntegtrationPoints(
      const ElementTypeMap<Int> &nb_integration_points_per_elem) {
    nb_averaging = nb_integration_points_per_elem;
  }

  Int getNbComponent(Int nb_component, ElementType type,
                     GhostType ghost_type) const {
    return nb_component / nb_averaging(type, ghost_type);
  }

private:
  ElementTypeMap<Int> nb_averaging;
};

int main(int argc, char *argv[]) {
  const Int dim = 2;

  initialize("material.dat", argc, argv);

  Mesh mesh(dim);

  const auto &comm = Communicator::getWorldCommunicator();
  Int prank = comm.whoAmI();
  if (prank == 0) {
    mesh.read("mesh.msh");
  }

  mesh.distribute();

  SolidMechanicsModel model(mesh);
  auto material_selector =
      std::make_shared<MeshDataMaterialSelector<std::string>>("physical_names",
                                                              model);
  model.setMaterialSelector(material_selector);
  model.initFull();

  auto &&support = make_support(mesh);
  auto &&steel = support->addSubSupport(mesh.getElementGroup("steel"));
  auto &&copper = support->addSubSupport(mesh.getElementGroup("copper"));

  support->addField("displacement", model.getDisplacement());
  copper.addField("stress", model.getMaterial("copper").getStress());

  auto &steel_strain = model.getMaterial("steel").getGradU();
  steel.addField("strain", steel_strain, average<Real>());

  auto &copper_strain = model.getMaterial("copper").getGradU();
  copper.addField("strain", copper_strain);

  DumperXdmf dumper(*support);

  dumper.dump();

  Vector<Real, dim> alpha{0.2, 1};
  for (auto &&[pos, dis, block] : zip(make_view<dim>(mesh.getNodes()),
                                      make_view<dim>(model.getDisplacement()),
                                      make_view<dim>(model.getBlockedDOFs()))) {
    dis.array() = alpha.array() * pos.array();
    block.set(true);
  }

  model.solveStep();
  //  MeshAccessor(mesh).getRelease() += 1;

  dumper.dump();

  // auto &&domain = XdmfDomain::New();

  // auto &&geometry = XdmfGeometry::New();
  // geometry->setType(XdmfGeometryType::XY());
  // geometry->insert(0, mesh.getNodes().storage(), 2 * mesh.getNbNodes());

  // auto &&topology_triangle = XdmfTopology::New();
  // topology_triangle->setType(XdmfTopologyType::Triangle());
  // const auto &triangles = mesh.getConnectivity(_triangle_3);
  // topology_triangle->insert(0, triangles.storage(), triangles.size() * 3);

  // auto &&topology_quad = XdmfTopology::New();
  // topology_quad->setType(XdmfTopologyType::Quadrilateral());
  // const auto &quadrangles = mesh.getConnectivity(_quadrangle_4);
  // topology_quad->insert(0, quadrangles.storage(), quadrangles.size() * 4);

  // auto &&grid_triangle = XdmfUnstructuredGrid::New();
  // grid_triangle->setName("mesh::_triangle_3");
  // grid_triangle->setGeometry(geometry);
  // grid_triangle->setTopology(topology_triangle);

  // auto &&grid_quadrangle = XdmfUnstructuredGrid::New();
  // grid_quadrangle->setName("mesh::_quadrangle_4");
  // grid_quadrangle->setGeometry(geometry);
  // grid_quadrangle->setTopology(topology_quad);

  // auto &&grid_mesh = XdmfGridCollection::New();
  // grid_mesh->setName("mesh");
  // grid_mesh->setType(XdmfGridCollectionType::Spatial());
  // grid_mesh->insert(grid_triangle);
  // grid_mesh->insert(grid_quadrangle);

  // auto &&grid_mesh_0 = XdmfGridCollection::New();
  // grid_mesh_0->setName("mesh:0");
  // grid_mesh_0->setType(XdmfGridCollectionType::Spatial());
  // grid_mesh_0->insert(grid_mesh);
  // shared_ptr<XdmfInformation> i0 = XdmfInformation::New("Time", "0.0");
  // grid_mesh_0->insert(i0);

  // auto &&grid_mesh_1 = XdmfGridCollection::New();
  // grid_mesh_1->setName("mesh:1");
  // grid_mesh_1->setType(XdmfGridCollectionType::Spatial());
  // grid_mesh_1->insert(grid_mesh);
  // shared_ptr<XdmfInformation> i1 = XdmfInformation::New("Time", "1.0");
  // grid_mesh_1->insert(i1);

  // auto &&grid_all = XdmfGridCollection::New();
  // grid_all->setName("all");
  // grid_all->setType(XdmfGridCollectionType::Temporal());
  // grid_all->insert(grid_mesh_0);
  // grid_all->insert(grid_mesh_1);

  // domain->insert(grid_all);

  // auto &&writer = XdmfWriter::New("output.xmf");
  // domain->accept(writer);

  finalize();
  return 0;
}
