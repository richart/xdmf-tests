h = 0.1;

SetFactory("OpenCASCADE");

Rectangle(1) = {-0.5, -0.5, 0, 1, 1};

Mesh.CharacteristicLengthMin = h;
Mesh.CharacteristicLengthMax = h;
