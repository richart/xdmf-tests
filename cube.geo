h = 0.1;

SetFactory("OpenCASCADE");

Box(1) = {-0.5, -0.5, -0.5, 1, 1, 1};

Mesh.CharacteristicLengthMin = h;
Mesh.CharacteristicLengthMax = h;
