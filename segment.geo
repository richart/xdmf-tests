h = 0.1;

Point(1) = {-0.5, 0, 0};
Point(2) = {0.5, 0, 0};

Line(1) = {1, 2};

SetFactory("OpenCASCADE");

Mesh.CharacteristicLengthMin = h;
Mesh.CharacteristicLengthMax = h;
