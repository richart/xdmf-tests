#include <benchmark/benchmark.h>
#include <chrono>
#include <cmath>
#include <hdf5.h>
#include <highfive/highfive.hpp>
#include <memory>
#include <mesh.hh>
#include <mpi.h>
#include <typeindex>
#include <vector>
using namespace akantu;

namespace {

void mpi_allreduce(benchmark::State & state) {
  double max_elapsed_second;
  int rank, psize;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_rank(MPI_COMM_WORLD, &psize);

  std::vector<double> data(state.range(0));

  for (auto && v : data) {
    v = rank;
  }

  for (auto _ : state) {
    // Do the work and time it on each proc
    auto start = std::chrono::high_resolution_clock::now();

    MPI_Allreduce(MPI_IN_PLACE, data.data(), data.size(), MPI_DOUBLE, MPI_MAX,
                  MPI_COMM_WORLD);

    auto end = std::chrono::high_resolution_clock::now();
    // Now get the max time across all procs:
    // for better or for worse, the slowest processor is the one that is
    // holding back the others in the benchmark.
    auto const duration =
        std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
    auto elapsed_seconds = duration.count();
    MPI_Allreduce(&elapsed_seconds, &max_elapsed_second, 1, MPI_DOUBLE, MPI_MAX,
                  MPI_COMM_WORLD);

    state.SetIterationTime(max_elapsed_second);
  }
  state.SetBytesProcessed(state.range(0) * sizeof(double) * state.iterations() *
                          std::log2(psize));
  state.SetComplexityN(state.range(0) * sizeof(double) * std::log2(psize));
  state.counters["msg_size"] = state.range(0) * sizeof(double);
}

class DistributedArray : public ::benchmark::Fixture {
public:
  void SetUp(::benchmark::State & state) {
    MPI_Comm_rank(MPI_COMM_WORLD, &prank);
    MPI_Comm_size(MPI_COMM_WORLD, &psize);

    auto dim = state.range(0);
    auto nb_nodes = state.range(1);

    auto mesh_filename =
        "mesh_" + std::to_string(dim) + "_" + std::to_string(nb_nodes) + ".msh";
    //    std::cerr << prank << "/" << psize << " - " << mesh_filename <<
    //    std::endl;

    mesh = std::make_unique<akantu::Mesh>(dim);
    if (prank == 0) {
      mesh->read(mesh_filename);
    }

    mesh->distribute();
  }

  void TearDown(::benchmark::State & /*state*/) { mesh.reset(nullptr); }

  std::unique_ptr<akantu::Mesh> mesh;
  int prank, psize;
};

#define TIDX(x) std::type_index(typeid(x))

hid_t h5_datatype_id(const std::type_index & type_idx) {
  const static std::unordered_map<std::type_index, hid_t> type_ids{
      {TIDX(int), H5T_NATIVE_INT},
      {TIDX(unsigned int), H5T_NATIVE_UINT},
      {TIDX(int32_t), H5T_NATIVE_INT32},
      {TIDX(int64_t), H5T_NATIVE_INT64},
      {TIDX(uint32_t), H5T_NATIVE_UINT32},
      {TIDX(uint64_t), H5T_NATIVE_UINT64},
      {TIDX(bool), H5T_NATIVE_CHAR},
      {TIDX(float), H5T_NATIVE_FLOAT},
      {TIDX(double), H5T_NATIVE_DOUBLE},
  };

  return type_ids.at(type_idx);
}

#undef TIDX

decltype(auto) openfile(const std::string & h5_filename,
                        MPI_Info & file_info_template) {
  auto fapl_id = H5Pcreate(H5P_FILE_ACCESS);

  H5Pset_fapl_mpio(fapl_id, MPI_COMM_WORLD, file_info_template);
  H5Pset_all_coll_metadata_ops(fapl_id, true);
  H5Pset_libver_bounds(fapl_id, H5F_LIBVER_LATEST, H5F_LIBVER_LATEST);

  auto fid =
      H5Fcreate(h5_filename.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, fapl_id);

  H5Pset_sieve_buf_size(fapl_id, 262144);
  H5Pset_alignment(fapl_id, 524288, 262144);
  MPI_Info_set(file_info_template, "access_style", "write_once");
  MPI_Info_set(file_info_template, "collective_buffering", "true");
  MPI_Info_set(file_info_template, "cb_block_size", "1048576");
  MPI_Info_set(file_info_template, "cb_buffer_size", "4194304");

  return std::pair{fid, fapl_id};
}

} // namespace

BENCHMARK_DEFINE_F(DistributedArray, hdf5_contigous_hyperslabs)
(benchmark::State & state) {
  double max_elapsed_second;

  auto filter_local_nodes = [&](const auto & in, auto & filtered) {
    auto out = make_view(filtered, filtered.getNbComponent()).begin();
    for (auto && [node, in] : enumerate(make_view(in, in.getNbComponent()))) {
      if (mesh->isLocalOrMasterNode(node)) {
        *out = in;
        ++out;
      }
    }
  };

  hsize_t nb_nodes = mesh->getNbNodes();
  hsize_t nb_local_nodes = mesh->getNbLocalNodes();
  hsize_t nb_total_nodes = mesh->getNbGlobalNodes();
  hsize_t offset = nb_local_nodes;
  mesh->getCommunicator().exclusiveScan(offset);

  auto write_data = [&](hid_t fid, const std::string & name,
                        const auto & data_in) {
    using array_type = std::decay_t<decltype(data_in)>;
    using scalar_type = typename array_type::value_type;

    array_type data(nb_local_nodes, data_in.getNbComponent());

    filter_local_nodes(data_in, data);

    std::array<hsize_t, 2> offsets{hsize_t(offset), 0};
    std::array<hsize_t, 2> dims{nb_nodes, hsize_t(data.getNbComponent())};
    std::array<hsize_t, 2> local_dims{nb_local_nodes, dims[1]};
    std::array<hsize_t, 2> global_dims{nb_total_nodes, dims[1]};

    akantu::Array<int> data_int(nb_local_nodes, 1);
    filter_local_nodes(mesh->getGlobalNodesIds(), data_int);

    auto memory_space_id =
        H5Screate_simple(local_dims.size(), local_dims.data(), nullptr);
    auto data_space_id =
        H5Screate_simple(global_dims.size(), global_dims.data(), nullptr);
    auto file_space_id =
        H5Screate_simple(global_dims.size(), global_dims.data(), nullptr);

    H5Sselect_hyperslab(file_space_id, H5S_SELECT_SET, offsets.data(), nullptr,
                        local_dims.data(), nullptr);

    auto datatype_id = h5_datatype_id(typeid(scalar_type));
    auto dataset_id = H5Dcreate(fid, name.c_str(), datatype_id, data_space_id,
                                H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

    auto dxpl_id = H5Pcreate(H5P_DATASET_XFER);
    H5Pset_dxpl_mpio(dxpl_id, H5FD_mpio_xfer_t(state.range(2)));

    H5Dwrite(dataset_id, datatype_id, memory_space_id, file_space_id, dxpl_id,
             data.data());

    H5Sclose(data_space_id);
    H5Sclose(memory_space_id);
    H5Sclose(file_space_id);
    H5Dclose(dataset_id);
    H5Pclose(dxpl_id);
  };

  auto h5_filename = "test_" + std::to_string(state.range(0)) + "_" +
                     std::to_string(state.range(1)) + "_ch.h5";

  for (auto _ : state) {
    MPI_Info file_info_template;
    MPI_Info_create(&file_info_template);
    auto [fid, fapl_id] = openfile(h5_filename, file_info_template);
    // Do the work and time it on each proc
    auto start = std::chrono::high_resolution_clock::now();

    write_data(fid, "positions", mesh->getNodes());
    write_data(fid, "global_ids", mesh->getGlobalNodesIds());

    auto end = std::chrono::high_resolution_clock::now();

    auto const duration =
        std::chrono::duration_cast<std::chrono::duration<double>>(end - start);

    H5Pclose(fapl_id);
    H5Fclose(fid);
    MPI_Info_free(&file_info_template);

    auto elapsed_seconds = duration.count();
    MPI_Allreduce(&elapsed_seconds, &max_elapsed_second, 1, MPI_DOUBLE, MPI_MAX,
                  MPI_COMM_WORLD);

    state.SetIterationTime(max_elapsed_second);
  }
}

BENCHMARK_DEFINE_F(DistributedArray, hdf5_hyperslabs_union)
(benchmark::State & state) {
  double max_elapsed_second;

  auto h5_filename = "test_" + std::to_string(state.range(0)) + "_" +
                     std::to_string(state.range(1)) + "_hu.h5";
  hsize_t nb_nodes = mesh->getNbNodes();
  hsize_t nb_local_nodes = mesh->getNbLocalNodes();
  hsize_t nb_total_nodes = mesh->getNbGlobalNodes();
  hsize_t offset = nb_local_nodes;
  mesh->getCommunicator().exclusiveScan(offset);

  akantu::Array<hsize_t> slabs(0, 3);

  auto write_data = [&](hid_t fid, const std::string & name,
                        const auto & data_in) {
    using array_type = std::decay_t<decltype(data_in)>;
    using scalar_type = typename array_type::value_type;

    std::array<hsize_t, 2> offsets{0, 0};
    std::array<hsize_t, 2> dims{nb_nodes, hsize_t(data_in.getNbComponent())};
    std::array<hsize_t, 2> gdims{nb_total_nodes, dims[1]};

    auto memory_space_id = H5Screate_simple(dims.size(), dims.data(), nullptr);
    auto data_space_id = H5Screate_simple(gdims.size(), gdims.data(), nullptr);
    auto file_space_id = H5Screate_simple(gdims.size(), gdims.data(), nullptr);

    H5S_seloper_t select_op = H5S_SELECT_SET;
    std::array<hsize_t, 2> sdims{0, dims[1]};

    if (slabs.empty()) {
      H5Sselect_none(memory_space_id);
      H5Sselect_none(file_space_id);
    }

    for (auto && slab : make_view<3>(slabs)) {
      offsets[0] = slab[0];
      sdims[0] = slab[2];
      H5Sselect_hyperslab(memory_space_id, select_op, offsets.data(), nullptr,
                          sdims.data(), nullptr);

      offsets[0] = slab[1];
      H5Sselect_hyperslab(file_space_id, select_op, offsets.data(), nullptr,
                          sdims.data(), nullptr);

      select_op = H5S_SELECT_OR;
    }

    auto datatype_id = h5_datatype_id(typeid(scalar_type));
    auto dataset_id = H5Dcreate(fid, name.c_str(), datatype_id, data_space_id,
                                H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

    auto dxpl_id = H5Pcreate(H5P_DATASET_XFER);
    H5Pset_dxpl_mpio(dxpl_id, H5FD_mpio_xfer_t(state.range(2)));

    H5Dwrite(dataset_id, datatype_id, memory_space_id, file_space_id, dxpl_id,
             data_in.data());

    H5Sclose(data_space_id);
    H5Sclose(memory_space_id);
    H5Sclose(file_space_id);
    H5Dclose(dataset_id);
    H5Pclose(dxpl_id);
  };

  for (auto _ : state) {
    MPI_Info file_info_template;
    MPI_Info_create(&file_info_template);
    auto [fid, fapl_id] = openfile(h5_filename, file_info_template);

    // Do the work and time it on each proc
    auto start = std::chrono::high_resolution_clock::now();

    hsize_t lnode = 0;
    while (lnode < nb_nodes) {
      hsize_t length = 0;
      auto start = lnode;
      auto gnode_counter = mesh->getNodeGlobalId(lnode);
      hsize_t gstart = gnode_counter;
      while (gnode_counter == mesh->getNodeGlobalId(lnode) and
             mesh->isLocalOrMasterNode(lnode) and lnode < nb_nodes) {
        ++lnode;
        ++gnode_counter;
        ++length;
      }

      if (length > 0) {
        slabs.push_back({start, gstart, length});
      }

      if (not mesh->isLocalOrMasterNode(lnode) and lnode < nb_nodes) {
        ++lnode;
      }
    }

    write_data(fid, "positions", mesh->getNodes());
    write_data(fid, "global_ids", mesh->getGlobalNodesIds());

    auto end = std::chrono::high_resolution_clock::now();

    auto const duration =
        std::chrono::duration_cast<std::chrono::duration<double>>(end - start);

    H5Pclose(fapl_id);
    H5Fclose(fid);
    MPI_Info_free(&file_info_template);

    auto elapsed_seconds = duration.count();
    MPI_Allreduce(&elapsed_seconds, &max_elapsed_second, 1, MPI_DOUBLE, MPI_MAX,
                  MPI_COMM_WORLD);

    state.SetIterationTime(max_elapsed_second);
  }
}

BENCHMARK_DEFINE_F(DistributedArray, hdf5_hyperslabs_union_hi5)
(benchmark::State & state) {
  using namespace HighFive;
  double max_elapsed_second;

  auto h5_filename = "test_" + std::to_string(state.range(0)) + "_" +
                     std::to_string(state.range(1)) + "_hu.h5";
  hsize_t nb_nodes = mesh->getNbNodes();
  hsize_t nb_local_nodes = mesh->getNbLocalNodes();
  hsize_t nb_total_nodes = mesh->getNbGlobalNodes();
  hsize_t offset = nb_local_nodes;
  mesh->getCommunicator().exclusiveScan(offset);

  akantu::Array<hsize_t> slabs(0, 3);

  auto write_data = [&](File & file, const std::string & name,
                        const auto & data_in) {
    using array_type = std::decay_t<decltype(data_in)>;
    using scalar_type = typename array_type::value_type;

    size_t nb_comp = data_in.getNbComponent();
    HyperSlab mem_slab, file_slab;

    for (auto && slab : make_view<3>(slabs)) {
      mem_slab |= RegularHyperSlab({slab[0], 0}, {slab[2], nb_comp});
      file_slab |= RegularHyperSlab({slab[1], 0}, {slab[2], nb_comp});
    }

    DataSet dataset = file.createDataSet<scalar_type>(
        name, DataSpace(nb_total_nodes, nb_comp));
    DataSpace mem_space(nb_nodes, nb_comp);

    auto xfer_props = DataTransferProps{};
    xfer_props.add(UseCollectiveIO{});

    dataset.select(file_slab, mem_slab.apply(mem_space))
        .write(data_in.data(), xfer_props);
    auto dxpl_id = H5Pcreate(H5P_DATASET_XFER);
    H5Pset_dxpl_mpio(dxpl_id, H5FD_mpio_xfer_t(state.range(2)));
  };

  for (auto _ : state) {
    FileAccessProps fapl;
    // We tell HDF5 to use MPI-IO
    fapl.add(MPIOFileAccess{MPI_COMM_WORLD, MPI_INFO_NULL});
    fapl.add(MPIOCollectiveMetadata{});
    // Now we can create the file as usual.
    File file(h5_filename, File::Truncate, fapl);

    // Do the work and time it on each proc
    auto start = std::chrono::high_resolution_clock::now();

    hsize_t lnode = 0;
    while (lnode < nb_nodes) {
      hsize_t length = 0;
      auto start = lnode;
      auto gnode_counter = mesh->getNodeGlobalId(lnode);
      hsize_t gstart = gnode_counter;
      while (gnode_counter == mesh->getNodeGlobalId(lnode) and
             mesh->isLocalOrMasterNode(lnode) and lnode < nb_nodes) {
        ++lnode;
        ++gnode_counter;
        ++length;
      }

      if (length > 0) {
        slabs.push_back({start, gstart, length});
      }

      if (not mesh->isLocalOrMasterNode(lnode) and lnode < nb_nodes) {
        ++lnode;
      }
    }

    write_data(file, "positions", mesh->getNodes());
    write_data(file, "global_ids", mesh->getGlobalNodesIds());

    file.flush();
    auto end = std::chrono::high_resolution_clock::now();

    auto const duration =
        std::chrono::duration_cast<std::chrono::duration<double>>(end - start);

    auto elapsed_seconds = duration.count();
    MPI_Allreduce(&elapsed_seconds, &max_elapsed_second, 1, MPI_DOUBLE, MPI_MAX,
                  MPI_COMM_WORLD);

    state.SetIterationTime(max_elapsed_second);
  }
}

BENCHMARK_REGISTER_F(DistributedArray, hdf5_contigous_hyperslabs)
    ->Name("HDF5::contiguous_hyperslabs")
    ->UseManualTime()
    ->Args({3, 1195, H5FD_MPIO_INDEPENDENT})
    ->Args({3, 1195, H5FD_MPIO_COLLECTIVE})
    ->Args({2, 145, H5FD_MPIO_INDEPENDENT})
    ->Args({2, 145, H5FD_MPIO_COLLECTIVE})
    ->Args({1, 11, H5FD_MPIO_INDEPENDENT})
    ->Args({1, 11, H5FD_MPIO_COLLECTIVE});

BENCHMARK_REGISTER_F(DistributedArray, hdf5_hyperslabs_union)
    ->Name("HDF5::union_of_hyperslabs")
    ->UseManualTime()
    ->Args({3, 1195, H5FD_MPIO_INDEPENDENT})
    ->Args({3, 1195, H5FD_MPIO_COLLECTIVE})
    ->Args({2, 145, H5FD_MPIO_INDEPENDENT})
    ->Args({2, 145, H5FD_MPIO_COLLECTIVE})
    ->Args({1, 11, H5FD_MPIO_INDEPENDENT})
    ->Args({1, 11, H5FD_MPIO_COLLECTIVE});

BENCHMARK_REGISTER_F(DistributedArray, hdf5_hyperslabs_union_hi5)
    ->Name("Hi5::union_of_hyperslabs")
    ->UseManualTime()
    ->Args({3, 1195, H5FD_MPIO_INDEPENDENT})
    ->Args({3, 1195, H5FD_MPIO_COLLECTIVE})
    ->Args({2, 145, H5FD_MPIO_INDEPENDENT})
    ->Args({2, 145, H5FD_MPIO_COLLECTIVE})
    ->Args({1, 11, H5FD_MPIO_INDEPENDENT})
    ->Args({1, 11, H5FD_MPIO_COLLECTIVE});

// BENCHMARK(mpi_allreduce)
//     ->Name("MPI::AllReduce")
//     ->UseManualTime()
//     ->RangeMultiplier(2)
//     ->Range(1, 1024)
//     ->Complexity(benchmark::oN);

// This reporter does nothing.
// We can use it to disable output from all but the root process
class NullReporter : public ::benchmark::BenchmarkReporter {
public:
  NullReporter() {}
  virtual bool ReportContext(const Context &) { return true; }
  virtual void ReportRuns(const std::vector<Run> &) {}
  virtual void Finalize() {}
};

// The main is rewritten to allow for MPI initializing and for selecting a
// reporter according to the process rank
int main(int argc, char ** argv) {

  MPI_Init(&argc, &argv);

  ::akantu::initialize(argc, argv);

  int rank, psize;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &psize);

  ::benchmark::Initialize(&argc, argv);

  if (rank == 0)
    // root process will use a reporter from the usual set provided by
    // ::benchmark
    ::benchmark::RunSpecifiedBenchmarks();
  else {
    // reporting from other processes is disabled by passing a custom reporter
    NullReporter null;
    ::benchmark::RunSpecifiedBenchmarks(&null);
  }

  MPI_Finalize();
  return 0;
}
